#Requires -RunAsAdministrator

# Check if winget is available.
if (!(Get-Command winget -ErrorAction SilentlyContinue)) {
    # Install winget.
    Invoke-WebRequest -Uri https://aka.ms/getwinget -OutFile winget.msixbundle
    Invoke-WebRequest -Uri https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx -OutFile Microsoft.VCLibs.x64.Desktop.appx
    Invoke-WebRequest -Uri https://github.com/microsoft/microsoft-ui-xaml/releases/download/v2.8.6/Microsoft.UI.Xaml.2.8.x64.appx -OutFile Microsoft.UI.Xaml.x64.appx
    Add-AppxPackage .\Microsoft.VCLibs.x64.Desktop.appx
    Add-AppxPackage .\Microsoft.UI.Xaml.x64.appx
    Add-AppxPackage .\winget.msixbundle
    Remove-Item -Force .\winget.msixbundle
    Remove-Item -Force .\Microsoft.UI.Xaml.x64.appx
    Remove-Item -Force .\Microsoft.VCLibs.x64.Desktop.appx
}

# Check if git is available.
if (!(Get-Command git -ErrorAction SilentlyContinue)) {
    # Install git.
    winget install --accept-package-agreements --accept-source-agreements --exact --silent --id=Git.Git
    $env:Path = [System.Environment]::ExpandEnvironmentVariables([System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User"))
}

# Check if the git command is in the $env:Path.
$GitPath = Split-Path -Parent (Get-Command git).Source
if ($env:Path -notlike $GitPath) {
    $env:Path += ";$GitPath"
}

# Check if ruby is available.
if (!(Get-Command ruby -ErrorAction SilentlyContinue)) {
    # Install ruby.
    winget install --accept-package-agreements --accept-source-agreements --exact --silent --id=RubyInstallerTeam.Ruby.3.2
    $env:Path = [System.Environment]::ExpandEnvironmentVariables([System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User"))
}

# Check if the ruby command is in the $env:Path.
$RubyPath = Split-Path -Parent (Get-Command ruby).Source
if ($env:Path -notlike $RubyPath) {
    $env:Path += ";$RubyPath"
}

# Install the Ruby bundle command and install Asciidoctor requirements.
gem update --system --no-document
gem update --no-document
gem install asciidoctor asciidoctor-pdf rouge text-hyphen

# Check if Visual Studio Code is available.
if (!(Get-Command code -ErrorAction SilentlyContinue)) {
    # Install Visual Studio Code.
    winget install --accept-package-agreements --accept-source-agreements --exact --silent --id=Microsoft.VisualStudioCode
    $env:Path = [System.Environment]::ExpandEnvironmentVariables([System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User"))
}

# Check if the Visual Studio Code command is in the $env:Path.
$VsCodePath = Split-Path -Parent (Get-Command code).Source
if ($env:Path -notlike "*$VsCodePath*") {
    $env:Path += ";$VsCodePath"
}

# Install Visual Studio Code extensions
code --install-extension asciidoctor.asciidoctor-vscode
code --install-extension jebbs.plantuml
